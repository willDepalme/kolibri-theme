# Kolibri Desktop Theme

A dark theme for cinnamon

## About Kolibri

Kolibri is a dark theme that extends the Linux Mint Cinnamon "Mint-Y-Dark" one

![Thumbnail](cinnamon/thumbnail.png)

## Installation

Clone or download the project and place the folder in your themes folder

For all users :
```
/usr/share/themes/
```
For a specific user : 
```
~/.themes/
```

## Fix for Firefox

Kolibri is a gtk dark theme and can cause display problems in firefox browser. Here is a workaround ( [see this](https://wiki.archlinux.org/index.php/Firefox#Unreadable_input_fields_with_dark_GTK.2B_themes
) ).

Copy the content of the `firefox_fix/` folder to `~/.mozilla/firefox/xxxxxxx.default/chrome/`
